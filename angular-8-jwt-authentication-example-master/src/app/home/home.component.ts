﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { VE } from '@app/_models';
import { UserService, VEService, AuthenticationService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users: User[];
    ves: VE[];

    constructor(private userService: UserService, private veService: VEService) { }

    ngOnInit() {
        this.loading = true;
        this.veService.getAll().pipe(first()).subscribe(ves => {
            this.loading = false;
            this.ves = ves;
        });
    }
}