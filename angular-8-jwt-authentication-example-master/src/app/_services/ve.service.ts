import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { VE } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class VEService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<VE[]>(`${environment.apiUrl}/virtualentity`);
    }
}