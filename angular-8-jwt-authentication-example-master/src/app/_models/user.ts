﻿export class User {
    username: string;
    accountId: number;
    name: string;
    password: string;
    valid: boolean;
    token?: string;
}