/*

App Name: Salford Energy
App Id: 3c7d2f2a-ed61-482d-921f-8aacb60d60d7
Test Account 1: energyhouse@salford.edu
Test Account 2: joulehouse@salford.edu

*/

const https = require('https');

var options = {
  'method': 'POST',
  'hostname': 'api.glowmarkt.com',
  'port': 443,
  'path': '/api/v0-1/auth',
  'headers': {
    'Content-Type': 'application/json'
  },
  'maxRedirects': 20
};

console.log("Running Auth API call")

var req = https.request(options, function (res) {
    var chunks = [];
  
    res.on("data", function (chunk) {
      chunks.push(chunk);
    });
  
    res.on("end", function (chunk) {
      var body = Buffer.concat(chunks);
      console.log(body.toString());
    });
  
    res.on("error", function (error) {
      console.error(error);
    });
  });
  
  var postData = JSON.stringify({"username":"joulehouse@glowtest.com","password":"","applicationId":"3c7d2f2a-ed61-482d-921f-8aacb60d60d7"});
  
  req.write(postData);
  
  req.end();